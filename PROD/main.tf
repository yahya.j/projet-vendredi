terraform {
  backend "s3" {

  }
}

provider "aws"{
        region = "us-east-1"
}

##Launch Configuration
resource "aws_launch_configuration" "Nono-Web" {
        image_id    ="ami-0728b67a5aa6b9487"
        instance_type = "t2.micro"
        security_groups = ["${aws_security_group.Nono-SecGroup.id}"]
}

##Auto scaling group
data "aws_availability_zones" "available" {

}

resource "aws_autoscaling_group" "Nono-ASgroup" {
	launch_configuration = "${aws_launch_configuration.Nono-Web.id}"
	availability_zones = ["${data.aws_availability_zones.available.names[0]}","${data.aws_availability_zones.available.names[1]}","${data.aws_availability_zones.available.names[2]}"]

	load_balancers		= ["${aws_elb.Nono-LB.name}"]
	health_check_type 	= "ELB"

	min_size = 3
	max_size = 10

	tag {
	 key			= "Name"
	 value			= "terraform-asg-Nono-ASgroup"
	 propagate_at_launch 	= true
	}
}


##Load Balancer

resource "aws_elb" "Nono-LB" {
	name			= "terraform-asg-Nono-LB"
	availability_zones = ["${data.aws_availability_zones.available.names[0]}"]
	security_groups		= ["${aws_security_group.Nono-LB-SecGroup.id}"]

	listener {
		lb_port			= "${var.server_port}"
		lb_protocol		= "http"
		instance_port		= "${var.server_port}"
		instance_protocol	= "http"
	}

	health_check {
		healthy_threshold	= 2
		unhealthy_threshold	= 2
		timeout			= 3
		interval		= 30
		target			= "HTTP:${var.server_port}/health.html"
	}
}


##Security Group

resource "aws_security_group" "Nono-SecGroup" {
  name = "terraform-example-Nono-SecGroup"

  ingress {
        from_port  = "${var.server_port}"
        to_port    = "${var.server_port}"
        protocol   = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
        }
}

resource "aws_security_group" "Nono-LB-SecGroup" {
  name = "terraform-example-Nono-SecGroup-LB"

  ingress {
        from_port  = "${var.server_port}"
        to_port    = "${var.server_port}"
        protocol   = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
        }
}

variable  "server_port" {
        description = "the port server will use for HTTP requets"
        default = "80"
}


output "elb_dns_name" {
        value = "${aws_elb.Nono-LB.dns_name}"
}
